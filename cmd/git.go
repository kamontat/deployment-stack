// Copyright © 2019 Kamontat Chantrachirathumrong <kamontat.c@hotmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"fmt"
	"os"
	"os/exec"

	"github.com/spf13/cobra"
)

var message string

// gitCmd represents the git command
var gitCmd = &cobra.Command{
	Use:   "git",
	Short: "add and commit changes",
	Run: func(cmd *cobra.Command, args []string) {
		// fmt.Println("git called")
		fmt.Print(message)

		command := exec.Command("git", "add", ".")
		command.Stdout = os.Stdout
		command.Stderr = os.Stderr
		command.Stdin = os.Stdin

		err := command.Run()
		if err != nil {
			fmt.Printf("Failed to complete git-add. %s\n", err.Error())
			os.Exit(1)
		}

		command1 := exec.Command("git", "commit", "-m", message)
		command1.Stdout = os.Stdout
		command1.Stderr = os.Stderr
		command1.Stdin = os.Stdin

		err = command1.Run()
		if err != nil {
			fmt.Printf("Failed to complete git-commit. %s\n", err.Error())
			os.Exit(1)
		}
	},
}

func init() {
	rootCmd.AddCommand(gitCmd)

	gitCmd.Flags().StringVarP(&message, "message", "m", "chore(release): update new version", "custom commit message")
}

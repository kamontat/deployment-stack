// Copyright © 2019 Kamontat Chantrachirathumrong <kamontat.c@hotmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"

	"github.com/spf13/cobra"
)

// locCmd represents the loc command
var locCmd = &cobra.Command{
	Use:   "loc",
	Short: "create line of code file",
	Run: func(cmd *cobra.Command, args []string) {
		if !isCommandAvailable("cloc") {
			log.Fatal("cloc is not exist, Installation: https://github.com/AlDanial/cloc")
		}

		command := exec.Command("cloc", "--md", ".")
		stdout, err := command.Output()
		if err != nil {
			log.Fatalln(err)
			os.Exit(1)
		}

		response := fmt.Sprintf("%s\n%s", "## Version "+version, string(stdout))

		f, err := os.OpenFile("LOC.md", os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			log.Fatalln(err)
			os.Exit(1)
		}
		raw, err := ioutil.ReadFile("LOC.md")
		previous := string(raw)
		f.WriteString(fmt.Sprintf("%s\n%s", response, previous))
	},
}

func init() {
	rootCmd.AddCommand(locCmd)

	locCmd.Flags().StringVarP(&version, "tag", "t", "", "custom next tag")
}

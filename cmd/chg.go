// Copyright © 2019 Kamontat Chantrachirathumrong <kamontat.c@hotmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"fmt"
	"log"
	"os"
	"os/exec"

	"github.com/spf13/cobra"
)

var version string

// locCmd represents the loc command
var chgCmd = &cobra.Command{
	Use:       "chg [init|create]",
	Short:     "create changelog file",
	Args:      cobra.OnlyValidArgs,
	ValidArgs: []string{"init", "create"},
	Run: func(cmd *cobra.Command, args []string) {
		if !isCommandAvailable("git-chglog") {
			log.Fatal("git-chglog is not exist, Installation: https://github.com/git-chglog/git-chglog")
		}

		if args[0] == "init" {
			// fmt.Println("Start initial")
			command := exec.Command("git-chglog", "--init")
			command.Stdout = os.Stdout
			command.Stderr = os.Stderr
			command.Stdin = os.Stdin

			err := command.Run()
			if err != nil {
				fmt.Printf("Failed to start Ruby. %s\n", err.Error())
				os.Exit(1)
			}
			// fmt.Println("Completed")
		} else if args[0] == "create" {
			var args []string
			args = append(args, "--output")
			args = append(args, "CHANGELOG.md")
			if version != "" {
				args = append(args, "--next-tag")
				args = append(args, version)
			}

			command := exec.Command("git-chglog", args...)
			command.Stdout = os.Stdout
			command.Stderr = os.Stderr

			err := command.Run()
			if err != nil {
				fmt.Printf("Failed to start Ruby. %s\n", err.Error())
				os.Exit(1)
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(chgCmd)

	chgCmd.Flags().StringVarP(&version, "tag", "t", "", "custom next tag")
}

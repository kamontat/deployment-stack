<a name="unreleased"></a>
## [Unreleased]


<a name="1.0.1"></a>
## [1.0.1] - 2019-02-04
### Build
- **bin:** create executable bin file

### Feat
- **version:** add version to cli


<a name="1.0.0"></a>
## 1.0.0 - 2019-02-04
### Chore
- **release:** update new version


[Unreleased]: https://gitlab.com/kamontat/deployment-stack/compare/1.0.1...HEAD
[1.0.1]: https://gitlab.com/kamontat/deployment-stack/compare/1.0.0...1.0.1

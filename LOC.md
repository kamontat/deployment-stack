## Version 1.0.1

cloc|github.com/AlDanial/cloc v 1.78  T=0.02 s (663.5 files/s, 28427.1 lines/s)
--- | ---

Language|files|blank|comment|code
:-------|-------:|-------:|-------:|-------:
Go|8|57|147|215
Markdown|4|19|0|91
YAML|1|0|11|17
--------|--------|--------|--------|--------
SUM:|13|76|158|323

## Version 1.0.0

cloc|github.com/AlDanial/cloc v 1.78  T=0.02 s (694.7 files/s, 31204.9 lines/s)
--- | ---

Language|files|blank|comment|code
:-------|-------:|-------:|-------:|-------:
Go|8|58|150|219
Markdown|3|12|0|72
YAML|1|0|11|17
--------|--------|--------|--------|--------
SUM:|12|70|161|308


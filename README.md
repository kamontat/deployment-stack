# Deployment stack

This document contains deployment on different situation and languages and stack

## Full deployment on nodeJS

1. Preparation
    - change version on package.json
2. Creation
    - `CHANGELOG` file
    - `LOC` file
3. Commition
    - create commit message as release notes
    - git `add` and git `commit`
4. Tag
    - create `tag` in git
5. Deployment
    - create release note on `Github release` (on Github)
    - create deployment on website `production`
    - create library on `npmjs.com/package`
